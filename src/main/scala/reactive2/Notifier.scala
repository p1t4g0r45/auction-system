package reactive2

import akka.Done
import akka.actor.{Actor, Props}
import akka.event.LoggingReceive
import akka.pattern.ask
import akka.util.Timeout

import scala.annotation.tailrec
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.Try

/**
  * Created by baran on 16.11.16.
  */
class Notifier extends Actor {

  override def receive: Receive = LoggingReceive {
    case n@Notify(_, _, _) => context.system.actorOf(Props[NotifierRequest]) ! n
  }
}

class NotifierRequest extends Actor {
  implicit val timeout = Timeout(5 seconds)
  val auctionPublish = context.actorSelection("akka.tcp://Reactive2@127.0.0.1:2553/user/publisher")

  override def receive: Receive = LoggingReceive {
    case n@Notify(_, _, _) =>
      @tailrec def send(i: Int): Unit = {
        println(s"sending message, try count: $i")
        val future = auctionPublish ? n
        Try(Await.result(future, timeout.duration))
        if (!future.isCompleted) send(i + 1) else context.stop(self)
      }
      send(1)
  }
}