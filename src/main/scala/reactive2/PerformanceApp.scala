package reactive2

import akka.actor.{Actor, ActorSystem, Props}
import akka.event.LoggingReceive
import com.typesafe.config.ConfigFactory

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.Random

/**
  * Created by baran on 18.11.16.
  */
class Main extends Actor {
  val N = 50000
  val M = 50000
  val auctions = List.fill(N) {
    java.util.UUID.randomUUID().toString
  }

  override def receive: Receive = LoggingReceive {
    case "init" =>
      context.actorOf(Props.create(classOf[Seller], auctions), "seller0")

    case "done" =>
      val buyer = context.system.actorOf(Props.create(classOf[Buyer]), "buyer")

      val start = System.currentTimeMillis()
      for {
        i <- 1 to M
        random = auctions(Random.nextInt(N))
      } {
        buyer ! BuyMessage(random, 15)
      }
      println(System.currentTimeMillis() - start)
      context.system.terminate()

    /**
      * 1 - 2007
      * 2 - 1267
      * 3 - 1735
      * 4 - 2253
      * 5 - 2152
      * 6 - 2138
      * 7 - 2211
      * 8 - 2015
      * 9 - 2121
      * 10 - 1596
      * 12 - 1936
      * 100 - 2573
      * 1000 - 19368
      */
  }
}

object PerformanceApp extends App {
  val config = ConfigFactory.load()

  val system = ActorSystem("Reactive2", config.getConfig("actorSystem").withFallback(config))
  val auctionSearch = system.actorOf(Props.create(classOf[MasterSearch]), "AuctionSearch")

  val notifier = system.actorOf(Props[Notifier], "notifier")
  val main = system.actorOf(Props.create(classOf[Main]), "main")


  Thread.sleep(100)
  main ! "init"

  Await.result(system.whenTerminated, Duration.Inf)
}
