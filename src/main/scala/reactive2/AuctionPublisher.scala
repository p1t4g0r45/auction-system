package reactive2

import akka.Done
import akka.actor.Actor
import akka.event.LoggingReceive

import scala.concurrent.forkjoin.ThreadLocalRandom

/**
  * Created by baran on 16.11.16.
  */
class AuctionPublisher extends Actor {
  override def receive: Receive = LoggingReceive {
    case Notify(name, bidder, bid) =>
      if (ThreadLocalRandom.current().nextDouble() < 0.5) {
        println(s"Auction Publisher: ($name) ${bidder.path.name} bid $bid")
        sender() ! Done
      }
  }
}
