package reactive2

import akka.actor.{Actor, ActorRef, ActorSelection, ActorSystem, Props}
import akka.event.LoggingReceive
import com.typesafe.config.ConfigFactory

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/**
  * Created by baran on 21.10.16.
  */


class Buyer extends Actor {

  val maxBid: Int = 10
  val name: String = context.self.path.name

  override def receive: Receive = LoggingReceive {
    case BuyMessage(auctionName, bid) =>
      context.system.actorSelection("/user/AuctionSearch") ! LookUp(auctionName, bid)
    /*
    bid only one
     */
    case LookUpResult(auction, bid) if auction.nonEmpty => auction.head ! Bid(bid)
    case HigherOffer(bid) if bid < maxBid => sender() ! Bid(bid + 1)
    case ReturnMessage(msg) => println(s"$name: $msg")
  }
}

class Seller(auctions: List[String]) extends Actor {

  val name: String = context.self.path.name

  auctions.foreach(auctionName => {
    val auction: ActorRef = context.actorOf(Props.create(classOf[Auction]), auctionName)
    auction ! Start()
    context.system.actorSelection("/user/AuctionSearch") ! Register(auctionName, auction)
  })
  context.parent ! "done"


  override def receive: Receive = LoggingReceive {
    case AuctionEnded(auctionName) => println(s"$name: auction $auctionName ended")
  }
}

class AuctionSearch extends Actor {

  var auctions: Map[String, ActorRef] = Map()

  override def receive: Receive = LoggingReceive {
    case Register(auctionName, actorRef) => auctions = auctions + (auctionName -> actorRef)
    case LookUp(auctionName, bid) => sender() ! LookUpResult(auctions.filter(_._1.contains(auctionName)).values.toList, bid)
  }
}


object AuctionApp extends App {
  val config = ConfigFactory.load()

  val auctionPublisher = ActorSystem("Reactive2", config.getConfig("auctionPublisher").withFallback(config))
  val publisherActor = auctionPublisher.actorOf(Props[AuctionPublisher], "publisher")

  val system = ActorSystem("Reactive2", config.getConfig("actorSystem").withFallback(config))
  val auctionSearch = system.actorOf(Props.create(classOf[MasterSearch]), "AuctionSearch")
  val auctions = List("abc", "def", "ghi")
  val seller0 = system.actorOf(Props.create(classOf[Seller], auctions), "seller0")
  val buyer0 = system.actorOf(Props.create(classOf[Buyer]), "buyer0")
  val buyer1 = system.actorOf(Props.create(classOf[Buyer]), "buyer1")
  val buyer2 = system.actorOf(Props.create(classOf[Buyer]), "buyer2")
  val notifier = system.actorOf(Props[Notifier], "notifier")


  Thread.sleep(100)
  buyer1 ! BuyMessage("abc", 4)
  buyer2 ! BuyMessage("abc", 5)
  buyer0 ! BuyMessage("abc", 3)

  Await.result(system.whenTerminated, Duration.Inf)
}

