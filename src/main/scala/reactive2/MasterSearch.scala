package reactive2

import akka.actor.{Actor, Props}
import akka.event.LoggingReceive
import akka.routing._
import scala.concurrent.duration._

/**
  * Created by baran on 18.11.16.
  *
  * RoundRobin:
  * 1: 11,218 | 8,182 | 11,731
  * 2: 10,128 | 10,662 | 11,040
  * 3: 10,800 | 10,215 | 10,622
  * 4: 3,785 | 6,897 | 9,565 | 11,244 | 6,919
  * 6: 11,087 | 8,816 | 13,043
  * 10: 10,864 | 10,140 | 9,856
  * 100: 10,031 | 10,235
  * 1000: 60+
  *
  * ScatterGatherFirstCompletedRouting:
  * 1: 7,032 | 6,861 | 3,186 | 9,787
  * 4: 10,220 | 7,963
  * 8: 7,768 | 10,264
  * 100: 10,828 | 12,068
  * 1000: 60+
  *
  */
class MasterSearch extends Actor {
  val nbOfroutees: Int = 1000

  val routees = Vector.fill(nbOfroutees) {
    val r = context.actorOf(Props[AuctionSearch])
    context watch r
    ActorRefRoutee(r)
  }

  var registerRouter = Router(BroadcastRoutingLogic(), routees)

  var lookUpRouter = Router(ScatterGatherFirstCompletedRoutingLogic(1 second), routees)


  def receive = LoggingReceive {
    case r: Register =>
      registerRouter.route(r, sender)
    case r: LookUp =>
      lookUpRouter.route(r, sender)
  }
}
