package reactive2

import java.util.Calendar
import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorRef, ActorSelection, FSM}
import akka.event.LoggingReceive
import akka.persistence.fsm.AbstractPersistentLoggingFSM

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.reflect._


/**
  * Created by baran on 04.11.16.
  */
class Auction extends FSM[State, Data] {

  val expireTime = 30000
  val deleteTime = 30000

  val name: String = context.self.path.name
  val seller: ActorRef = context.parent
  startWith(NotCreated, Data(null, 0, 0))

  when(NotCreated) {
    case Event(Start(), _) =>
      setBidTimer()
      goto(Created) using  stateData.copy(endDate = Calendar.getInstance().getTimeInMillis + expireTime)
  }

  when(Created) {
    case Event(Bid(newBid), _) =>
      sender ! ReturnMessage("Your bid is valid, activating auction")
      sendNotify(sender(), newBid)
      goto(Activated) using Data(sender(), newBid, stateData.endDate)
    case Event(Expire(), _) =>
      setDeleteTimer()
      goto(Ignored) using stateData.copy(endDate = Calendar.getInstance().getTimeInMillis + deleteTime)
  }

  when(Ignored) {
    case Event(ReList(), _) =>
      setBidTimer()
      goto(Created) using stateData.copy(endDate = Calendar.getInstance().getTimeInMillis + expireTime)
    case Event(Delete(), _) => delete()
  }

  when(Activated) {
    case Event(Bid(newBid), _) if stateData.bid < newBid =>
      sender ! ReturnMessage(s"Your bid ($newBid) is valid")
      stateData.bidder ! HigherOffer(newBid)
      sendNotify(sender(), newBid)
      goto(Activated) using Data(sender(), newBid, stateData.endDate)
    case Event(Bid(newBid), _) =>
      sender ! ReturnMessage("Your bid is not valid")
      goto(Activated)
    case Event(Expire(), _) =>
      println("\n--------------\nEnded\n--------------\n")
      stateData.bidder ! ReturnMessage(s"You bought $name for ${stateData.bid}")
      setDeleteTimer()
      goto(Ended) using stateData.copy(endDate = Calendar.getInstance().getTimeInMillis + deleteTime)
    case Event(GetData(), _) => {
      sender() ! DataResult(stateData)
      goto(Activated)
    }
  }

  when(Ended) {
    case Event(Delete(), _) => delete()
  }

  when(Deleted) {
    case _ => goto(Deleted)
  }

  def sendNotify(bidder: ActorRef, bid: Int) = {
    val notifier: ActorSelection = context.actorSelection("/user/notifier")
    notifier ! Notify(name, bidder, bid)
  }

  def delete() = {
    seller ! AuctionEnded(name)
    context.stop(self)
    goto(Deleted)
  }

  def setBidTimer(): Unit = setBidTimer(deleteTime)

  def setDeleteTimer(): Unit = setDeleteTimer(deleteTime)

  def setBidTimer(time: Long): Unit = context.system.scheduler
    .scheduleOnce(time millis)(self ! Expire())

  def setDeleteTimer(time: Long): Unit = context.system.scheduler
    .scheduleOnce(time millis)(self ! Delete())

}



class Auction3 extends AbstractPersistentLoggingFSM[State, Data, Data] {

  val expireTime = 30000
  val deleteTime = 30000

  val name: String = context.self.path.name
  val seller: ActorRef = context.parent
  startWith(NotCreated, Data(null, 0, 0))

  when(NotCreated) {
    case Event(Start(), _) =>
      setBidTimer()
      goto(Created) applying stateData.copy(endDate = Calendar.getInstance().getTimeInMillis + expireTime)
  }

  when(Created) {
    case Event(Bid(newBid), _) =>
      sender ! ReturnMessage("Your bid is valid, activating auction")
      goto(Activated) applying Data(sender(), newBid, stateData.endDate)
    case Event(Expire(), _) =>
      setDeleteTimer()
      goto(Ignored) applying stateData.copy(endDate = Calendar.getInstance().getTimeInMillis + deleteTime)
  }

  when(Ignored) {
    case Event(ReList(), _) =>
      setBidTimer()
      goto(Created) applying stateData.copy(endDate = Calendar.getInstance().getTimeInMillis + expireTime)
    case Event(Delete(), _) => delete()
  }

  when(Activated) {
    case Event(Bid(newBid), _) if stateData.bid < newBid =>
      sender ! ReturnMessage(s"Your bid ($newBid) is valid")
      stateData.bidder ! HigherOffer(newBid)
      goto(Activated) applying Data(sender(), newBid, stateData.endDate)
    case Event(Bid(newBid), _) =>
      sender ! ReturnMessage("Your bid is not valid")
      goto(Activated)
    case Event(Expire(), _) =>
      println("\n--------------\nEnded\n--------------\n")
      stateData.bidder ! ReturnMessage(s"You bought $name for ${stateData.bid}")
      setDeleteTimer()
      goto(Ended) applying stateData.copy(endDate = Calendar.getInstance().getTimeInMillis + deleteTime)
    case Event(GetData(), _) => {
      sender() ! DataResult(stateData)
      goto(Activated)
    }
  }

  when(Ended) {
    case Event(Delete(), _) => delete()
  }

  when(Deleted) {
    case _ => goto(Deleted)
  }



  def delete() = {
    seller ! AuctionEnded(name)
    context.stop(self)
    goto(Deleted)
  }

  def setBidTimer(): Unit = setBidTimer(deleteTime)

  def setDeleteTimer(): Unit = setDeleteTimer(deleteTime)

  def setBidTimer(time: Long): Unit = context.system.scheduler
    .scheduleOnce(time millis)(self ! Expire())

  def setDeleteTimer(time: Long): Unit = context.system.scheduler
    .scheduleOnce(time millis)(self ! Delete())


  override def domainEventClassTag: ClassTag[Data] = classTag[Data]

  override def applyEvent(domainEvent: Data, currentData: Data): Data = domainEvent

  override def onRecoveryCompleted() = {
    super.onRecoveryCompleted
    println("recover")
    val l: Long = stateData.endDate - Calendar.getInstance().getTimeInMillis
    if (l > 0)
      stateName match {
        case Created | Activated => setBidTimer(l)
        case Ignored | Ended => setDeleteTimer(l)
        case _ =>
      }
  }

  override def persistenceId: String = s"persistent-auction-$name"
}

/// old

class Auction2 extends Actor {

  var bid = 0
  var bidder: ActorRef = _
  val name: String = context.self.path.name
  val seller: ActorRef = context.parent

  def receive = LoggingReceive {
    case Start() =>
      setBidTimer()
      context become created
  }

  def created: Receive = LoggingReceive {
    case Expire() =>
      setDeleteTimer()
      context become ignored

    case Bid(amount: Int) if amount > bid =>
      sender ! ReturnMessage("Your bid is valid, activating auction")
      bid = amount
      bidder = sender
      context become activated

  }

  def ignored: Receive = LoggingReceive {
    case Delete() =>
      delete()

    case ReList() =>
      setBidTimer()
      context become created

  }

  def activated: Receive = LoggingReceive {
    case Bid(amount: Int) if amount > bid =>
      sender ! ReturnMessage(s"Your bid ($amount) is valid")
      bidder ! HigherOffer(amount)
      bid = amount
      bidder = sender

    case Bid(amount: Int) =>
      sender ! ReturnMessage("Your bid is not valid")

    case Expire() =>
      setDeleteTimer()
      bidder ! ReturnMessage(s"You bought $name for $bid")
      context become soldReceive

  }

  def soldReceive: Receive = LoggingReceive {
    case Delete() =>
      delete()

  }

  def delete() = {
    seller ! AuctionEnded(name)
    context stop self
  }

  def setBidTimer(): Unit = context.system.scheduler
    .scheduleOnce(FiniteDuration(5, TimeUnit.SECONDS))(self ! Expire())

  def setDeleteTimer(): Unit = context.system.scheduler
    .scheduleOnce(FiniteDuration(5, TimeUnit.SECONDS))(self ! Delete())

}