package reactive2

import java.util.Date

import akka.actor.ActorRef
import akka.persistence.fsm.PersistentFSM.FSMState

/**
  * Created by baran on 04.11.16.
  */
class States
// Auction message types
case class Bid(bid: Int)

case class BuyMessage(auctionName: String, bid: Int)

case class ReturnMessage(msg: String)

case class ReList()

case class Expire()

case class Delete()

case class Start()

// Auction states
sealed trait State extends FSMState

case object NotCreated extends State {
  override def identifier: String = "not-created"
}

case object Created extends State {
  override def identifier: String = "created"
}

case object Ignored extends State {
  override def identifier: String = "ignored"
}

case object Activated extends State {
  override def identifier: String = "activated"
}

case object Ended extends State {
  override def identifier: String = "ended"
}

case object Deleted extends State {
  override def identifier: String = "deleted"
}

// Auction data

final case class Data(bidder: ActorRef, bid: Int, endDate: Long)

// AuctionSearch message types
case class Register(auctionName: String, auction: ActorRef)

case class LookUp(auctionName: String, bid: Int)

case class LookUpResult(auction: List[ActorRef], bid: Int)

// Seller message types
case class AuctionEnded(auctionName: String)

//Buyer message types
case class HigherOffer(bid: Int)

// Notifier
case class Notify(auctionName: String, bidderName: ActorRef, bid: Int)

//test
case class GetData()
case class DataResult(data: Data)