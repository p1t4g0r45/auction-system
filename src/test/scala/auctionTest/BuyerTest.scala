package auctionTest

import akka.actor.{ActorSystem, Props}
import akka.testkit.{TestActorRef, TestKit, TestProbe}
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}
import reactive2._

/**
  * Created by baran on 30.10.16.
  */
class BuyerTest extends TestKit(ActorSystem("BuyerTest"))
  with WordSpecLike with BeforeAndAfterAll {

  override def afterAll(): Unit = {
    system.terminate
  }

  val buyer = TestActorRef[Buyer]
  val auction = TestProbe()

  "A buyer" should {

    "bid higher when he can" in {
      auction.send(buyer, HigherOffer(5))
      auction.expectMsg(Bid(6))
    }

    "not bid higher when he can't" in {
      auction.send(buyer, HigherOffer(50))
      auction.expectNoMsg()
    }

  }
}
