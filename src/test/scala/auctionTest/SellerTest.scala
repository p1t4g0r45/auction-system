package auctionTest

import akka.actor.Actor.Receive
import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.event.LoggingReceive
import akka.testkit.{TestActorRef, TestKit, TestProbe}
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}
import reactive2._

import scala.concurrent.duration._

/**
  * Created by baran on 30.10.16.
  */
class SellerTest extends TestKit(ActorSystem("SellerTest"))
  with WordSpecLike with BeforeAndAfterAll {

  override def afterAll(): Unit = {
    system.terminate
  }

  val auctionSearch = TestProbe()
  system.actorOf(Props.create(classOf[Helper], auctionSearch.ref), name = "AuctionSearch")
  val seller = system.actorOf(Props.create(classOf[Seller], List("abc", "bcd")), "seller")


  "A seller" should {

    "register auction" in {
      auctionSearch.expectMsgClass(1 second, classOf[Register])
      auctionSearch.expectMsgClass(1 second, classOf[Register])
    }
  }
}

class Helper(auctionSearch: ActorRef) extends Actor {
  override def receive: Receive = LoggingReceive { case a => auctionSearch ! a }
}