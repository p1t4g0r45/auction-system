package auctionTest

import akka.actor.{ActorSystem, Props}
import akka.testkit.{TestActorRef, TestKit, TestProbe}
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}
import reactive2._

/**
  * Created by baran on 30.10.16.
  */
class AuctionSearchTest extends TestKit(ActorSystem("AuctionSearchTest"))
  with WordSpecLike with BeforeAndAfterAll {

  override def afterAll(): Unit = {
    system.terminate
  }

  "A seller" should {
    val auctionSearch = TestActorRef[AuctionSearch](Props.create(classOf[AuctionSearch]), "auctionSearch")
    val seller = TestProbe()
    val auction = TestProbe()
    val buyer = TestProbe()

    "accept new auctions" in {
      assert(auctionSearch.underlyingActor.auctions.isEmpty)
      seller.send(auctionSearch, Register("test", auction.testActor))
      assert(auctionSearch.underlyingActor.auctions("test") == auction.testActor)
    }

    "lookup existing auctions" in {
      buyer.send(auctionSearch, LookUp("test", 4))
      buyer.expectMsg(LookUpResult(List(auction.testActor), 4))
    }

    "return empty list on no result found" in {
      buyer.send(auctionSearch, LookUp("no-test", 42))
      buyer.expectMsg(LookUpResult(List(), 42))
    }

  }

}
