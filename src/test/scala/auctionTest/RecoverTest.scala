package auctionTest

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.event.LoggingReceive
import akka.testkit.TestKit
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}
import reactive2._

import scala.concurrent.Await
import scala.concurrent.duration._

/**
  * Created by baran on 30.10.16.
  */
class RecoverTest extends TestKit(ActorSystem("AuctionTest"))
  with WordSpecLike with BeforeAndAfterAll {

  override def afterAll(): Unit = {
    system.terminate
  }

  "An auction" should {


    "recover" in {
      val system = ActorSystem("Test")
      val auctionSearch1 = system.actorOf(Props.create(classOf[AuctionSearch]), "AuctionSearch")
      val auctions = List("abc")
      val seller1 = system.actorOf(Props.create(classOf[Seller], auctions), "seller1")
      val buyer1 = system.actorOf(Props.create(classOf[Buyer]), "buyer1")
      val test1 = system.actorOf(Props.create(classOf[TestActor], auctionSearch1), "test")


      Thread.sleep(100)
      buyer1 ! BuyMessage("abc", 4)
      Await.result(system.terminate(), 10 seconds)


      val system2 = ActorSystem("Test")
      val auctionSearch = system2.actorOf(Props.create(classOf[AuctionSearch]), "AuctionSearch")
      val seller2 = system2.actorOf(Props.create(classOf[Seller], auctions), "seller1")
      val buyer2 = system2.actorOf(Props.create(classOf[Buyer]), "buyer1")
      val test = system2.actorOf(Props.create(classOf[TestActor2], auctionSearch), "test")

      test ! "init"
      Thread.sleep(1000)

    }
  }
}

object Global {
  var endTime: Long = 0
}

class TestActor(search: ActorRef) extends Actor {

  override def receive: Receive = LoggingReceive {
    case "init" => search ! LookUp("abc", 0)
    case LookUpResult(l, _) => l.head ! GetData()
    case DataResult(data) => Global.endTime = data.endDate
  }
}

class TestActor2(search: ActorRef) extends Actor {

  override def receive: Receive = LoggingReceive {
    case "init" => search ! LookUp("abc", 0)
    case LookUpResult(l, _) => l.head ! GetData()
    case DataResult(data) => assert(Global.endTime == data.endDate)
  }
}