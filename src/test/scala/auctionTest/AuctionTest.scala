package auctionTest

import akka.actor.{ActorSystem, Props}
import akka.testkit.{TestActorRef, TestKit, TestProbe}
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}
import reactive2._

import scala.concurrent.Await
import scala.concurrent.duration._

/**
  * Created by baran on 30.10.16.
  */
class AuctionTest extends TestKit(ActorSystem("AuctionTest"))
  with WordSpecLike with BeforeAndAfterAll {

  override def afterAll(): Unit = {
    system.terminate
  }

  "An auction" should {
    val seller = TestProbe()
    val buyer1 = TestProbe()
    val buyer2 = TestProbe()
    val auction = TestActorRef[Auction](Props.create(classOf[Auction]), seller.testActor, "auctionName")

    "not be started at the beginning" in {
      assert(auction.underlyingActor.stateName == NotCreated)
    }

    "be started after signal" in {
      auction ! Start()
      assert(auction.underlyingActor.stateName == Created)
    }

    "accept any first bid" in {
      buyer1.send(auction, Bid(5))
      buyer1.expectMsg(ReturnMessage("Your bid is valid, activating auction"))
      assert(auction.underlyingActor.stateData.bid == 5)
    }

    "accept any higher bid and inform previous bidder" in {
      buyer2.send(auction, Bid(10))
      buyer1.expectMsg(HigherOffer(10))
      buyer2.expectMsg(ReturnMessage("Your bid (10) is valid"))
      assert(auction.underlyingActor.stateData.bid == 10)
    }

    "not accept any lower bid" in {
      buyer1.send(auction, Bid(3))
      buyer1.expectMsg(ReturnMessage("Your bid is not valid"))
      assert(auction.underlyingActor.stateData.bid == 10)
    }

  }

}
